/* Includes ----------------------------------------------------- */
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"

/* Function prototypes ------------------------------------------ */
void ION_RCC_GPIO_init(GPIO_TypeDef* GPIOx);
