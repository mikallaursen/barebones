/*
	Contains configuration functions and usage functions for the ADC.
	The code is written for the Sensor module 2016.
	
	GPIOA is used for the inputs.
	DMA2 stream0 channel0 is used for transferring data.
	TIM3 is used for triggering the ADC conversion with a 
	given frequency.
	
	The DMA2_Stream0_IRQHandler is used to create an interrupt when 
	the buffer is filled. The buffer is full when the ADC have
	sampled all the channels n times.
	
	In the DMA2_Stream0_IRQHandler the n-average of the individual channels are
	taken and put in the global buffer BSP_ADC_rawData. From there on the
  data is either sendt out on the CAN-bus or processed by a corresponsing 
  sensor handler(i.e converting to temperature).	
	
	The DMA stores the readings in the local variabel ADCBuffer.
*/

/* includes ---------------------------------------------------*/
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_tim.h"
#include "BSP_LED.h"
#include "ION_GPIO.h"
#include "stm32f4xx_adc.h"

/* Local function prototypes ----------------------------------*/
static void ADC_GPIOInit(uint8_t nbrOfChannels);
static void ADC_DMAInit(uint32_t bufferSize);
static void ADC_NVICinit(void);
static void ADC_init(uint8_t nbrOfChannels);
static void ADC_TIMInit(uint32_t samplingFrequency);
/* Variables --------------------------------------------------*/
/* ADC_channels available for the sensor Module */
uint8_t ADC_Channels[8] = 
{
	ADC_Channel_0, 
	ADC_Channel_1, 
	ADC_Channel_2, 
	ADC_Channel_3,
	ADC_Channel_4,
	ADC_Channel_5,
 	ADC_Channel_6,
	ADC_Channel_7
};
/* GPIOs used for the ADC on the Sesnor Module */
uint16_t ADC_GPIOs[8] =
{
	GPIO_Pin_0,
	GPIO_Pin_1,
	GPIO_Pin_2,
	GPIO_Pin_3,
	GPIO_Pin_4,
	GPIO_Pin_5,
	GPIO_Pin_6,
	GPIO_Pin_7,
};

uint32_t ADC_bufferSize;
uint16_t ADC_oversamplingValue;
uint8_t ADC_nbrOfChannels;

volatile uint16_t ADCBuffer[10000];
/* Constant variables----------------------------------------- */
enum{MAX_SAMPLING_FREQUENCY = 200000};

/* Init functions -------------------------------------------- */


/**
* @brief Configures n-channels with a common frequency and oversampling rate.
				 The sampling frequency is given by frequency*oversampling. The maximum sampling
					frequency is 200000 hertz.
* @input nbrOfChannel : The number of channels to configure, can be a number between 1 and 8.
* @input frequency 		: The output frequency of the channels. Can be a number between 10 and 
												20000. Given in hertz.
* @input oversampling : The oversampling rate. Can be a number between 1 and 1000.
*/
void BSP_ADC_init(uint8_t nbrOfChannels, uint32_t frequency, uint16_t oversampling)
{	
	
	ADC_bufferSize = nbrOfChannels*oversampling;
	ADC_oversamplingValue = oversampling;
	ADC_nbrOfChannels = nbrOfChannels;
	
	ADC_GPIOInit(nbrOfChannels);
	ADC_init(nbrOfChannels);
	ADC_NVICinit();
	ADC_TIMInit(oversampling*frequency);
	ADC_DMAInit(ADC_bufferSize);
}

static void ADC_GPIOInit(uint8_t nbrOfChannels)
{
	GPIO_InitTypeDef				GPIO_initStruct;
	uint8_t k = 0;
	uint16_t GPIO_pins = 0;
	/* Reset the GPIO Structure */
	GPIO_StructInit(&GPIO_initStruct);
	/* Enable the GPIO clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, 	ENABLE);
	
	/* Mask the wanted GPIO channels */
	for(k=0; k<nbrOfChannels; k++)
	{
		GPIO_pins |= ADC_GPIOs[k];
	}
	/* Configurate GPIO */
	GPIO_initStruct.GPIO_Mode     = GPIO_Mode_AN;
	GPIO_initStruct.GPIO_Pin      = GPIO_pins;
	GPIO_initStruct.GPIO_PuPd     = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_OType    = GPIO_OType_PP;
	GPIO_initStruct.GPIO_Speed    = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIO_initStruct);
}

/**  
 @brief DMA configuration. See table 43 in DM00031020 for DMA mapping.
 Uses DMA2, DMA channel 0 and stream 0. The DMA transfers data to the 
 local bufferADCbuffer.
*/
static void ADC_DMAInit(uint32_t bufferSize)
{
	DMA_InitTypeDef DMA_initStruct;
	DMA_StructInit(&DMA_initStruct);
	/* Enable the DMA2 clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, 	ENABLE);
	/* Configure DMA channel */
	DMA_initStruct.DMA_Channel            = DMA_Channel_0; 
	DMA_initStruct.DMA_BufferSize         = bufferSize;
	DMA_initStruct.DMA_DIR                = DMA_DIR_PeripheralToMemory;
	DMA_initStruct.DMA_FIFOMode           = DMA_FIFOMode_Disable;
	DMA_initStruct.DMA_FIFOThreshold      = 0;
	DMA_initStruct.DMA_MemoryBurst        = DMA_MemoryBurst_Single;
	DMA_initStruct.DMA_Mode               = DMA_Mode_Circular;
	DMA_initStruct.DMA_Priority           = DMA_Priority_High;
	DMA_initStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	/* Configure DMA memory */
	DMA_initStruct.DMA_Memory0BaseAddr    = (uint32_t)ADCBuffer;
	DMA_initStruct.DMA_MemoryDataSize     = DMA_MemoryDataSize_HalfWord;
	DMA_initStruct.DMA_MemoryInc          = DMA_MemoryInc_Enable;
	DMA_initStruct.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;
	DMA_initStruct.DMA_PeripheralInc      = DMA_PeripheralInc_Disable;
	DMA_Init(DMA2_Stream0, &DMA_initStruct); 
	/* Enable the transfer complete interrupt. */
	DMA_ITConfig(DMA2_Stream0, DMA_IT_TC, ENABLE);
	
	DMA_Cmd(DMA2_Stream0, ENABLE);
}

static void ADC_NVICinit()
{
	NVIC_InitTypeDef NVIC_initStruct;
	/* Configure and enable the NVIC for the ADC_IRQHandler*/
	NVIC_initStruct.NVIC_IRQChannel                    = DMA2_Stream0_IRQn;
	NVIC_initStruct.NVIC_IRQChannelCmd                 = ENABLE;
	NVIC_initStruct.NVIC_IRQChannelPreemptionPriority  = 0;
	NVIC_initStruct.NVIC_IRQChannelSubPriority         = 0;
	NVIC_Init(&NVIC_initStruct);
}

/** 
 @brief Configures TIM3 to trigger the adc at a given frequency.
 @Input frequency 		: Desired sampling frequency given in hertz. 
 @Input oversampling : Desired oversampling.
*/
static void ADC_TIMInit(uint32_t samplingFrequency)
{
	// TODO : Handle to high sampling frequency
	TIM_TimeBaseInitTypeDef TIM_initStruct;
	TIM_TimeBaseStructInit(&TIM_initStruct);
	/* Enable the TIM3 clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	/* Configure TIM */
	TIM_initStruct.TIM_Period         = (uint16_t)49;
	TIM_initStruct.TIM_Prescaler      = 420;
	TIM_initStruct.TIM_ClockDivision  = TIM_CKD_DIV1;
	TIM_initStruct.TIM_CounterMode    = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_initStruct);
	/* Configure TIM3 to trigger the ADC */
	TIM_SelectOutputTrigger(TIM3, TIM_TRGOSource_Update);
	TIM_Cmd(TIM3, ENABLE);
}

/**
* @brief configures the ADC and ADCcommon
*/
static void ADC_init(uint8_t nbrOfChannels)
{
	ADC_InitTypeDef ADC_initStruct;
	ADC_CommonInitTypeDef ADC_commonInitStruct;
	uint8_t k = 0;
	/* Resets the initstructures */
	ADC_StructInit(&ADC_initStruct);
	ADC_CommonStructInit(&ADC_commonInitStruct);
	/* Enable clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	/* Configure ADC in independent mode */
	ADC_commonInitStruct.ADC_DMAAccessMode    = ADC_DMAAccessMode_Disabled;
	ADC_commonInitStruct.ADC_Mode             = ADC_Mode_Independent;
	ADC_commonInitStruct.ADC_Prescaler        = ADC_Prescaler_Div2;
	ADC_commonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_commonInitStruct);

	/* Configure the ADC: 12-bit single conversion */
	ADC_initStruct.ADC_Resolution             = ADC_Resolution_12b;
	ADC_initStruct.ADC_ScanConvMode           = ENABLE;
	ADC_initStruct.ADC_ContinuousConvMode     = DISABLE;
	ADC_initStruct.ADC_ExternalTrigConvEdge   = ADC_ExternalTrigConvEdge_Rising;
	ADC_initStruct.ADC_ExternalTrigConv       = ADC_ExternalTrigConv_T3_TRGO;
	ADC_initStruct.ADC_DataAlign              = ADC_DataAlign_Right;
	ADC_initStruct.ADC_NbrOfConversion        = nbrOfChannels;

	ADC_Init(ADC1, &ADC_initStruct);
	
	/* Configure the regular channel */
	for(k=0; k < nbrOfChannels; k++)
	{
		ADC_RegularChannelConfig(ADC1, ADC_Channels[k], (k+1) , ADC_SampleTime_144Cycles);
	}
	ADC_Cmd(ADC1, ENABLE);
	ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);
	ADC_DMACmd(ADC1, ENABLE);
} /* END OF ADC_INIT */


/**
 @brief For reading the value of a single channel 
 @input channel : the channel to be read. given by @defgroup ADC_channels 
									in stm32f4xx_adc.h
*/
uint16_t BSP_ADC_readChannel(uint16_t channel)
{
	ADC_RegularChannelConfig(ADC1, channel, 1, ADC_SampleTime_480Cycles);
	ADC_SoftwareStartConv(ADC1);
	while((ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET));
	return ADC_GetConversionValue(ADC1);
}

uint32_t ADC_averagedRawData[8];
/* IRQ Handlers */
void DMA2_Stream0_IRQHandler(void)
{	
	uint8_t n;
	uint8_t k;
	uint32_t tempAveragedRawData[8]={0,0,0,0,0,0,0,0};
 	DMA_ClearITPendingBit(DMA2_Stream0, DMA_IT_TC);
	/* Calculate the average value for each channel */
	for(n = 0; n < ADC_nbrOfChannels; n++)
	{
		for(k = 0; k <= ADC_bufferSize; k+=ADC_nbrOfChannels)
		{
			tempAveragedRawData[n] += ADCBuffer[k+n];
		}
		ADC_averagedRawData[n] = tempAveragedRawData[n]/ADC_oversamplingValue;
	}
}
void TIM2_IRQHandler()
{
	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	/* Do nothing */
}

